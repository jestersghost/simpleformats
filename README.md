simpleformats contains classes and functions for parsing text-based
file formats.

A format is broken down into (up to) three parts:

+ batch - the file structure
+ record - each entry in the batch (not applicable for xml/json)
+ field - each datum within a record (not applicable for xml/json)

eg. To define a .csv with only one line format:

```python
csv_record = delimited_record(
	( 'order', string_field() ),
	( 'amount', decimal_field() ),
	( 'quantity', int_field( positive = True ) ),
)
csv_batch = record_batch( single_record = csv_record )
```
